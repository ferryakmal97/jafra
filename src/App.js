import {
  BrowserRouter,
  Navigate,
  Route,
  Routes,
  useLocation,
} from "react-router-dom";
import "./App.css";
import AdminLayout from "./layouts/AdminLayout";
import NonAdminLayout from "./layouts/NonAdminLayout";
import {
  AddConsultantPage,
  AddProductPage,
  ChatPage,
  ConsultantChatPage,
  ConsultantPage,
  DashboardAdminPage,
  EditConsultantPage,
  EditProductPage,
  HomePage,
  ListConsultantPage,
  ListProductPage,
  LoginPage,
  ProductPage,
  RegisterPage,
} from "./pages";

function RequireAuth({ children }) {
  let user = JSON.parse(localStorage.getItem("user"));
  let location = useLocation();

  if (!user) {
    // Redirect them to the /login page, but save the current location they were
    // trying to go to when they were redirected. This allows us to send them
    // along to that page after they login, which is a nicer user experience
    // than dropping them off on the home page.
    return <Navigate to="/login" state={{ from: location }} replace />;
  }
  return children;
}

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route
          path="/"
          element={
            <RequireAuth>
              <NonAdminLayout />
            </RequireAuth>
          }
        >
          <Route path="/" element={<HomePage />} />
          <Route path="/products" element={<ProductPage />} />
          <Route path="/consultants" element={<ConsultantPage />} />
          <Route path="/chat" element={<ChatPage />} />
          <Route path="/consultant-chat" element={<ConsultantChatPage />} />
        </Route>
        <Route
          path="admin"
          element={
            <RequireAuth>
              <AdminLayout />
            </RequireAuth>
          }
        >
          <Route path="" element={<DashboardAdminPage />} />
          <Route path="product/add" element={<AddProductPage />} />
          <Route path="product/list" element={<ListProductPage />} />
          <Route path="product/edit/:id" element={<EditProductPage />} />
          <Route path="consultant/list" element={<ListConsultantPage />} />
          <Route path="consultant/add" element={<AddConsultantPage />} />
          <Route path="consultant/edit/:id" element={<EditConsultantPage />} />
        </Route>
        <Route path="/login" element={<LoginPage />} />
        <Route path="/register" element={<RegisterPage />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
