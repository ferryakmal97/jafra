import { MenuFoldOutlined, MenuUnfoldOutlined } from "@ant-design/icons";
import { woman } from "@assets";
import { colors } from "@utils";
import { Button, Typography } from "antd";
import React, { useState } from "react";

const { Title } = Typography;

const Header = ({ title = "Home" }) => {
  const [visible, setVisible] = useState(false);
  return (
    <div style={styles.container}>
      <Button onClick={() => setVisible(!visible)}>
        {React.createElement(visible ? MenuUnfoldOutlined : MenuFoldOutlined)}
      </Button>
      <Title style={{ color: colors.white, marginBottom: 0 }}>{title}</Title>
      <img src={woman} alt="Logo" style={styles.image} />
    </div>
  );
};

export default Header;

const styles = {
  container: {
    backgroundColor: colors.primary,
    height: "12vh",
    width: "100vw",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    padding: 30,
  },
  image: {
    width: 50,
    height: 50,
    backgroundColor: "white",
    borderRadius: 50,
  },
};
