// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore, collection } from "firebase/firestore";
import { getFirestore as getLite } from "firebase/firestore/lite";
import { getStorage } from "firebase/storage";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyBVlSUa6SvkPGbF2godVoJZ0NY_cdnrQL4",
  authDomain: "jafra-web.firebaseapp.com",
  projectId: "jafra-web",
  storageBucket: "jafra-web.appspot.com",
  messagingSenderId: "304730719304",
  appId: "1:304730719304:web:e72f98e2339dacfd72047d",
  measurementId: "G-JH8WCGNTEY",
};

// Initialize Firebase
const FIREBASE = initializeApp(firebaseConfig);
export const auth = getAuth(FIREBASE);
export const dbLite = getLite(FIREBASE);
export const storage = getStorage(FIREBASE)

export const db = getFirestore(FIREBASE);
export const productCollectionRef = collection(db, "products");
export const userCollectionRef = collection(db, "users");

export default FIREBASE;
