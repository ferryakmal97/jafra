export const getBase64 = (file) => {
    return new Promise((resolve, reject) => {
        const reader = new FileReader()

        reader.readAsDataURL(file)
        reader.onload = () => resolve(reader.result)
        reader.onerror = (err) => reject(err)
    })
}

export const linkToImage = async (link) => {
    const res = await fetch(link)
    const blob = await res.blob()

    const file = new File([blob], "product-image.jpg", {type: "image/jpeg"})

    return [file]
} 