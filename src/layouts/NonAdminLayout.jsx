import { Button, Dropdown, Layout, Menu, message } from "antd";
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  FileOutlined,
  LogoutOutlined,
  UserOutlined,
  UsergroupAddOutlined,
  DashboardOutlined,
} from "@ant-design/icons";
import { useState } from "react";
import { css } from "@emotion/css";
import { colors } from "../utils";
import { Outlet, Link, useNavigate } from "react-router-dom";
import { signOut } from "firebase/auth";
import { auth } from "../config";

const { Header, Sider, Content } = Layout;

const NonAdminLayout = () => {
  const [collapsed, setCollapsed] = useState(false);
  const onToggle = () => setCollapsed((prev) => !prev);
  const user = JSON.parse(localStorage.getItem("user"));

  const navigate = useNavigate();

  const onLogout = () => {
    signOut(auth)
      .then(() => {
        localStorage.clear();
        message.success("Logout Success !");
        navigate("/login", { replace: true });
      })
      .catch((e) => {
        console.log("error", e);
      });
  };

  const menu = (
    <Menu>
      <Menu.Item icon={<LogoutOutlined />} onClick={onLogout}>
        Logout
      </Menu.Item>
    </Menu>
  );

  return (
    <Layout
      className={css`
        height: 100vh;
      `}
    >
      <Sider
        trigger={null}
        collapsible
        collapsed={collapsed}
        theme="light"
        collapsedWidth={0}
      >
        <Menu
          mode="inline"
          defaultSelectedKeys={["1"]}
          theme="light"
          className={css`
            margin-top: 60px;
          `}
        >
          <Menu.Item key="1" icon={<DashboardOutlined />}>
            <Link to="./">Dashboard</Link>
          </Menu.Item>
          <Menu.Item key="2" icon={<FileOutlined />}>
            <Link to="products">Produk</Link>
          </Menu.Item>
          {user.role !== "CONSULTANT" && (
            <Menu.Item key="3" icon={<UsergroupAddOutlined />}>
              <Link to="consultants">Konsultan</Link>
            </Menu.Item>
          )}
        </Menu>
      </Sider>
      <Layout>
        <Header
          className={css`
            background-color: ${colors.primary};
            display: flex;
            justify-content: space-between;
            align-items: center;
          `}
        >
          {collapsed ? (
            <MenuUnfoldOutlined
              onClick={() => onToggle()}
              className={css`
                font-size: 18px;
                color: ${colors.white};
              `}
            />
          ) : (
            <MenuFoldOutlined
              onClick={() => onToggle()}
              className={css`
                font-size: 18px;
                color: ${colors.white};
              `}
            />
          )}
          <Dropdown overlay={menu} placement="bottomRight" trigger={["click"]}>
            <Button
              icon={<UserOutlined />}
              size="large"
              shape="circle"
              color="#fffff"
            />
          </Dropdown>
        </Header>
        <Content
          className={css`
            margin: 24px 16px;
            padding: 24px;
            background-color: ${colors.white};
          `}
        >
          <Outlet />
        </Content>
      </Layout>
    </Layout>
  );
};

export default NonAdminLayout;
