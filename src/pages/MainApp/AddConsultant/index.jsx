import { 
    Button, 
    Form,
    Input,
    message,
    Typography
} from "antd"
import { createUserWithEmailAndPassword } from "firebase/auth"
import { useState } from "react"
import { useNavigate } from "react-router-dom"
import { auth } from "../../../config"
import { addDataFixId } from "../../../utils"

const AddConsultantPage = () => {
    const [loading, setLoading] = useState(false)

    const [form] = Form.useForm()

    const navigate = useNavigate()

    const onRegisterConsultant = (val) => {
        setLoading(true)

        const { email, password, consultantName, phoneNumber } = val

        createUserWithEmailAndPassword(auth, email, password)
            .then((userCredential) => {
                const user = userCredential.user

                const data = {
                    email,
                    consultantName,
                    phoneNumber,
                    consultantID: user.uid,
                    consultant_created: user.metadata.creationTime,
                    role: "CONSULTANT"
                }

                addDataFixId("users", user.uid, data)

                message.success("Konsultan berhasil ditambahkan")

                setLoading(false)

                navigate("/admin/consultant/list")
            }).catch(() => {
                setLoading(false)

                message.error("Konsultan gagal ditambahkan")
            })
    }

    return (
        <>
            <Typography.Title level={3}>Tambah Konsultan</Typography.Title>
            <Form
                form={form}
                layout="vertical"
                initialValues={{
                    consultantName: "",
                    email: "",
                    password: "",
                    phoneNumber: ""
                }}
                onFinish={onRegisterConsultant}
            >
                <Form.Item
                    name="consultantName"
                    label="Nama"
                    rules={[{
                        required: true,
                        message: "Tidak boleh kosong"
                    }]}
                    hasFeedback
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    name="phoneNumber"
                    label="Nomor Telepon"
                    rules={[{
                        required: true,
                        message: "Tidak boleh kosong"
                    }]}
                    hasFeedback
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    name="email"
                    label="Email"
                    rules={[
                        {
                            required: true,
                            message: "Tidak boleh kosong,"
                        },
                        {
                            type: "email",
                            message: "Email tidak valid"
                        }
                    ]}
                    hasFeedback
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    name="password"
                    label="Password"
                    rules={[
                        {
                            required: true,
                            message: "Tidak boleh kosong,"
                        },
                    ]}
                    hasFeedback
                >
                    <Input type="password" />
                </Form.Item>
                <Form.Item>
                    <Button 
                        type="primary" 
                        htmlType="submit"
                        loading={loading}
                        disabled={loading}
                    >
                        Tambah Konsultan
                    </Button>
                </Form.Item>
            </Form>
        </>
    )
}

export default AddConsultantPage