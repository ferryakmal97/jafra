import { 
    Button, 
    Form,
    Input,
    Upload,
    InputNumber,
    Typography,
    Switch,
    message
} from "antd"
import { css } from "@emotion/css";
import { useState } from "react";
import { getBase64 } from "../../../helper/helper";
import { createDoc } from "../../../utils";
import { productCollectionRef, storage } from "../../../config";
import { getDownloadURL, ref, uploadBytes } from "firebase/storage";
import { useNavigate } from "react-router-dom";
import { doc } from "firebase/firestore";

const AddProductPage = () => {
    const [form] = Form.useForm()
    const [previewImage, setPreviewImage] = useState("")
    const [loading, setLoading] = useState(false)

    const navigate = useNavigate()

    const onChange = async (e) => {
        const img = await getBase64(e.file)

        setPreviewImage(img)
    }

    return (
        <>
            <Typography.Title level={3}>Tambah Produk</Typography.Title>
            <Form
                form={form}
                layout="vertical"
                initialValues={{
                    productName:"",
                    productImage: [],
                    productPrice: null,
                    productDesc: "",
                    productActive: true
                }}
                onFinish={async (val) => {
                    setLoading(true)

                    const imgFile = val.productImage[0]["originFileObj"]

                    const docRef = doc(productCollectionRef)
                    const path = `${docRef.id}.jpg`
                    const storageRef = ref(storage,`files/${path}`)

                    const uploadImage = await uploadBytes(storageRef, imgFile)
                    const imgURL = await getDownloadURL(uploadImage.ref)

                    createDoc(docRef, {
                        productName: val.productName,
                        image_product: imgURL,
                        description: val.productDesc,
                        price: val.productPrice,
                        productID: docRef.id,
                        IS_ACTIVE: val.productActive
                    }).then(() => {
                        setLoading(false)

                        message.success("Produk berhasil ditambahkan")

                        navigate("/admin/product/list")

                    }) .catch(() => {
                        setLoading(false)

                        message.error("Produk gagal ditambahkan")
                    })
                    
                }}
            >
                <Form.Item
                    name="productName"
                    label="Nama Produk"
                    rules={[{
                        required: true,
                        message: "Tidak boleh kosong"
                    }]}
                    hasFeedback
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    name="productImage"
                    label="Foto Produk"
                    rules={[{
                        required: true,
                        message: "Tidak boleh kosong"
                    }]}
                    valuePropName="fileList"
                    getValueFromEvent={(e) => {
                        if(Array.isArray(e)) return e

                        return e && e.fileList
                    }}
                >
                    <Upload 
                        listType="picture-card"
                        onChange={onChange}
                        showUploadList={false}
                        beforeUpload={() => false}
                        multiple={false}
                        maxCount={1}
                    >
                        <div>
                            {
                                previewImage ? (
                                    <img 
                                        className={css`
                                            width: 100%;
                                        `}
                                        src={previewImage}
                                        alt="Image Preview"
                                    />
                                ) : "Unggah Foto"
                            }
                        </div>
                    </Upload>
                </Form.Item>
                <Form.Item
                    name="productDesc"
                    label="Deskripsi"
                    rules={[{
                        required: true,
                        message: "Tidak boleh kosong"
                    }]}
                    hasFeedback
                >
                    <Input.TextArea rows={4} />
                </Form.Item>
                <Form.Item
                    name="productPrice"
                    label="Harga"
                    rules={[
                        {
                            type:"number",
                            message: "Harus berupa angka"
                        },
                        {
                            required: true,
                            message: "Tidak boleh kosong"
                        }
                    ]}
                    hasFeedback
                >
                    <InputNumber
                        className={css`width: 100%;`}
                        formatter={value => `Rp ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                        parser={value => value.replace(/\Rp\s?|(,*)/g, '')}
                    />
                </Form.Item>
                <Form.Item
                    name="productActive"
                    label="Aktif"
                    valuePropName="checked"
                >
                    <Switch />
                </Form.Item>
                <Form.Item>
                    <Button 
                        type="primary" 
                        htmlType="submit"
                        loading={loading}
                        disabled={loading}
                    >
                        Simpan Produk
                    </Button>
                </Form.Item>
            </Form>
        </>
    )
}

export default AddProductPage