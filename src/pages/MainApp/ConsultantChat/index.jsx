import { SendOutlined, SolutionOutlined } from "@ant-design/icons";
import {
  Avatar,
  Button,
  Divider,
  Image,
  Input,
  List,
  Modal,
  Skeleton,
  Table,
  Tag,
  Typography,
} from "antd";
import { collection, onSnapshot, query, where } from "firebase/firestore";
import React, { Fragment, useEffect, useRef, useState } from "react";
import { woman } from "../../../assets";
import { db, productCollectionRef } from "../../../config";
import {
  convertDate,
  getDocById,
  getFromCollection,
  getHour,
  getWithQuery,
  sendMsg,
} from "../../../utils";

const { Title, Text } = Typography;
const { TextArea } = Input;

const ConsultantChatPage = () => {
  const [user, setUser] = useState(JSON.parse(localStorage.getItem("user")));
  const [users, setUsers] = useState([]);
  const [target, setTarget] = useState(null);
  const [text, setText] = useState("");
  const [message, setMessage] = useState({});
  const messageContainer = useRef(null);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [dataModal, setDataModal] = useState(null);

  const getDataModal = async () => {
    const data = await getFromCollection(productCollectionRef);
    setDataModal(data);
  };

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const scrollToBottom = () => {
    messageContainer.current.scrollTop = messageContainer.current.scrollHeight;
  };

  useEffect(() => {
    if (target) {
      const q = query(
        collection(db, "messages"),
        where("members", "in", [
          [user?.consultantID, target.customerID],
          [target.customerID, user?.consultantID],
        ]),
      );
      const unsubscribe = onSnapshot(q, (querySnapshot) => {
        const otherUser = [];
        querySnapshot.forEach((value) => {
          otherUser.push(value.data());
        });
        if (otherUser.length > 0) {
          setMessage(otherUser[0]);
          scrollToBottom();
        } else {
          setMessage({});
        }
      });
      return () => {
        unsubscribe();
      };
    }
  }, [target]);

  const getData = async () => {
    const data = await getWithQuery(
      "messages",
      "members",
      "array-contains",
      user.consultantID,
    );
    const listUsers = [];
    data.forEach((val) => {
      const users = val.members.find((value) => value !== user.consultantID);
      listUsers.push(users);
    });
    const users = [];
    for (let value of listUsers) {
      const data = await getDocById("users", value);
      users.push(data);
    }
    setUsers(users);
    setTarget(users[0]);
  };

  useEffect(() => {
    getData();
    getDataModal();
  }, []);

  const onSend = () => {
    const msg = {
      sender: user.consultantID,
      message: text,
      timestamp: new Date(),
    };
    sendMsg(msg, message.msgId);
    setText("");
  };

  const recommendProduct = (item) => {
    const msg = {
      sender: user.consultantID,
      message: item,
      type: "SUGGESTION",
      timestamp: new Date(),
    };
    sendMsg(msg, message.msgId);
    setText("");
    setIsModalVisible(false);
  };

  return (
    <div style={styles.container}>
      <div style={styles.listContainer}>
        <div>
          <Divider style={{ fontSize: 24 }}>Chat With</Divider>
        </div>
        <div style={styles.list}>
          {users.map((item, index) => (
            <Fragment key={index}>
              <div style={styles.listItem} onClick={() => setTarget(item)}>
                <img
                  alt="profile"
                  src={woman}
                  style={{ width: 50, height: 50, borderRadius: 50 }}
                />
                <div>
                  <Title level={4} style={{ margin: 0 }}>
                    {item.customerName}
                  </Title>
                  <p>{item.email}</p>
                </div>
              </div>
              {index !== users.length - 1 && <Divider style={{ margin: 0 }} />}
            </Fragment>
          ))}
        </div>
      </div>
      <div style={styles.chatContainer}>
        <div style={styles.messageContainer} ref={messageContainer}>
          {message?.messages?.map((value, index, array) => {
            const isMe = user.consultantID === value.sender;
            let countTime =
              index !== 0
                ? convertDate(array[index - 1].timestamp.toDate())
                : "";
            return (
              <Fragment key={index}>
                {countTime !== convertDate(value.timestamp.toDate()) && (
                  <Tag
                    style={{
                      display: "flex",
                      alignSelf: "center",
                      marginTop: index === 0 ? 10 : 0,
                    }}
                    color="lightpink"
                  >
                    {convertDate(value.timestamp.toDate())}
                  </Tag>
                )}
                {value?.type === "SUGGESTION" ? (
                  <div
                    style={{
                      ...styles.bubbleContainer,
                      backgroundColor: "lightpink",
                      alignSelf: "flex-end",
                      color: "white",
                    }}
                  >
                    <Image
                      src={value?.message?.image_product}
                      width={60}
                      style={{ borderRadius: 8 }}
                    />
                    <div
                      style={{
                        justifyContent: "center",
                        display: "flex",
                        flexDirection: "column",
                        marginLeft: 10,
                      }}
                    >
                      <Title level={5} style={{ color: "white" }}>
                        {value?.message?.productName}
                      </Title>
                      <Text style={{ color: "white" }}>
                        {value?.message?.description}
                      </Text>
                    </div>
                  </div>
                ) : (
                  <div
                    style={
                      isMe
                        ? {
                            ...styles.bubbleContainer,
                            backgroundColor: "lightpink",
                            alignSelf: "flex-end",
                            color: "white",
                          }
                        : {
                            ...styles.bubbleContainer,
                            backgroundColor: "white",
                            alignSelf: "flex-start",
                            borderStyle: "solid",
                            borderWidth: 1,
                            borderColor: "lightpink",
                          }
                    }
                  >
                    {value.message}
                  </div>
                )}
                <Text
                  style={{
                    display: "flex",
                    alignSelf: isMe ? "flex-end" : "flex-start",
                    marginBottom:
                      index === message?.messages?.length - 1 ? 20 : 0,
                  }}
                  type="secondary"
                >
                  {getHour(value.timestamp.toDate())}
                </Text>
              </Fragment>
            );
          })}
        </div>
        <Input.Group compact style={styles.inputContainer}>
          <TextArea
            style={{ width: "90%" }}
            placeholder="Type a message"
            autoSize={{ minRows: 1, maxRows: 6 }}
            onChange={(e) => setText(e.target.value)}
            value={text}
          />
          <Button
            type="primary"
            shape="circle"
            style={{ alignSelf: "center" }}
            icon={<SolutionOutlined />}
            onClick={showModal}
          />
          <Button
            type="primary"
            shape="circle"
            style={{ alignSelf: "center" }}
            icon={<SendOutlined />}
            onClick={onSend}
          />
        </Input.Group>
      </div>
      <Modal
        title="Suggest Products"
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        width={1000}
      >
        <List
          itemLayout="horizontal"
          dataSource={dataModal}
          renderItem={(item, index) => (
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                marginTop: index === 0 ? 0 : 16,
                alignItems: "center",
              }}
            >
              <List.Item.Meta
                avatar={<Avatar src={item.image_product} />}
                title={item.productName}
                description={item.description}
              />
              <div style={{ marginRight: 16 }}>Rp {item.price}</div>
              <Button
                type="primary"
                disabled={!item.IS_ACTIVE}
                onClick={() => recommendProduct(item)}
              >
                Recommend this !
              </Button>
            </div>
          )}
        />
      </Modal>
    </div>
  );
};

export default ConsultantChatPage;

const styles = {
  container: {
    display: "flex",
    flex: 1,
    height: "100%",
  },
  listContainer: {
    display: "flex",
    flexDirection: "column",
    flex: 0.3,
  },
  list: {
    overflow: "scroll",
    overflowX: "hidden",
  },
  listItem: {
    display: "flex",
    flexDirection: "row",
    padding: 10,
    alignItems: "center",
    cursor: "pointer",
  },
  chatContainer: {
    display: "flex",
    flexDirection: "column",
    flex: 0.7,
  },
  messageContainer: {
    display: "flex",
    flex: 1,
    flexDirection: "column",
    paddingRight: 20,
    paddingLeft: 20,
    overflow: "scroll",
    overflowX: "hidden",
  },
  bubbleContainer: {
    display: "flex",
    marginTop: 10,
    padding: 10,
    borderRadius: 8,
    maxWidth: "80%",
  },
  inputContainer: {
    display: "flex",
    alignSelf: "flex-end",
    backgroundColor: "white",
    padding: 16,
    justifyContent: "space-between",
  },
};
