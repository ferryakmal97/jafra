import React, { useState } from "react";
import { SendOutlined } from "@ant-design/icons";
import { Modal, Button, Tag } from "antd";
import { woman } from "../../../assets";

const Card = ({ data, style }) => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [selected, setSelected] = useState({});
  const showModal = () => {
    setSelected(data);
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setSelected({});
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setSelected({});
    setIsModalVisible(false);
  };
  return (
    <div
      style={{
        height: "35%",
        width: "15%",
        ...style,
      }}
    >
      <div
        style={{
          display: "flex",
          width: "100%",
          height: "100%",
          marginBottom: 10,
          position: "relative",
        }}
      >
        <img
          src={woman}
          style={{
            display: "flex",
            width: "100%",
            height: "100%",
          }}
          alt=""
        />
      </div>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <Button type="primary" block onClick={showModal}>
          {data.consultantName}
        </Button>
      </div>
      <>
        <Modal
          title={selected.consultantName}
          visible={isModalVisible}
          onOk={handleOk}
          onCancel={handleCancel}
        >
          <p>{selected.email}</p>
          <p>{selected.phoneNumber}</p>
        </Modal>
      </>
    </div>
  );
};

export default Card;
