import React, { useEffect, useState } from "react";
import { getWithQuery } from "../../../utils/services";
import Card from "./Card";

const ConsultantPage = () => {
  const [data, setDatas] = useState(null);

  const getData = async () => {
    const data = await getWithQuery("users", "role", "==", "CONSULTANT");
    setDatas(data);
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <div
      style={{
        display: "flex",
        flexWrap: "wrap",
        justifyContent: "center",
        paddingTop: 20,
        paddingBottom: 20,
      }}
    >
      {data &&
        data.map((value, index) => (
          <Card data={value} key={index} style={{ paddingRight: 10 }} />
        ))}
    </div>
  );
};

export default ConsultantPage;
