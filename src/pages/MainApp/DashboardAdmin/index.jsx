import { css } from "@emotion/css";
import { Card, Space, Statistic, Spin } from "antd";
import { StockOutlined, UserOutlined, TeamOutlined } from "@ant-design/icons";
import { query } from "firebase/firestore/lite";
import { productCollectionRef, userCollectionRef } from "../../../config";
import { getDocs, where } from "firebase/firestore";
import { useState } from "react";
import { useEffect } from "react";

const DashboardAdminPage = () => {
  const [totalConsultant, setTotalConsultant] = useState(0);
  const [totalProduct, setTotalProduct] = useState(0);
  const [totalCustomer, setTotalCustomer] = useState(0);
  const [loadingData, setLoadingData] = useState(false);

  const fetchDataLength = async () => {
    setLoadingData(true);

    const q1 = query(userCollectionRef, where("role", "==", "CONSULTANT"));
    const q2 = query(userCollectionRef, where("role", "==", "CUSTOMER"));

    const consultants = await getDocs(q1);
    const customers = await getDocs(q2);
    const products = await getDocs(productCollectionRef);

    setTotalConsultant(consultants.size);
    setTotalCustomer(customers.size);
    setTotalProduct(products.size);

    setLoadingData(false);
  };

  useEffect(() => fetchDataLength(), []);

  return (
    <div
      style={{
        display: "flex",
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        height: "100%",
      }}
    >
      {loadingData ? (
        <div
          className={css`
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100%;
          `}
        >
          <Spin />
        </div>
      ) : (
        <Space direction="horizontal">
          <Card>
            <Statistic
              title="Total Produk"
              prefix={<StockOutlined />}
              value={totalProduct}
            />
          </Card>
          <Card>
            <Statistic
              title="Total Customer"
              prefix={<UserOutlined />}
              value={totalCustomer}
            />
          </Card>
          <Card>
            <Statistic
              title="Total Konsultan"
              prefix={<TeamOutlined />}
              value={totalConsultant}
            />
          </Card>
        </Space>
      )}
    </div>
  );
};

export default DashboardAdminPage;
