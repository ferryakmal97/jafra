import { 
    Button, 
    Form,
    Input,
    message,
    Spin,
    Typography
} from "antd"
import { useEffect } from "react"
import { useState } from "react"
import { useNavigate, useParams } from "react-router-dom"
import { getDocById, updateDocById } from "../../../utils"
import { css } from "@emotion/css"

const EditConsultantPage = () => {
    const [loadingData, setLoadingData] = useState(false)
    const [loadingUpdate, setLoadingUpdate] = useState(false)

    const [form] = Form.useForm()

    const { id } = useParams()
    const navigate = useNavigate()

    const getConsultant = async () => {
        setLoadingData(true)
        
        const consultant = await getDocById("users", id)

        form.setFieldsValue({
            consultantName: consultant.consultantName,
            phoneNumber: consultant.phoneNumber
        })

        setLoadingData(false)
    }

    useEffect(() => {
        getConsultant()
    },[])

    return (
        <>
            {
                loadingData ? (
                    <div
                        className={css`
                            display: flex;
                            justify-content: center;
                            align-items: center;
                            height: 100%;
                        `}
                    >
                        <Spin />
                    </div>
                ) : (
                    <>
                        <Typography.Title level={3}>Edit Konsultan</Typography.Title>
                        <Form
                            form={form}
                            layout="vertical"
                            initialValues={{
                                consultantName: "",
                                phoneNumber: ""
                            }}
                            onFinish={(val) => {
                                setLoadingUpdate(true)

                                updateDocById("users", id, {
                                    consultantName: val.consultantName,
                                    phoneNumber: val.phoneNumber
                                }).then(() => {
                                    setLoadingUpdate(false)

                                    message.success("Data konsultan berhasil diperbarui")

                                    navigate("/admin/consultant/list")
                                }).catch(() => {
                                    setLoadingUpdate(false)

                                    message.error("Data konsultan gagal diperbarui")
                                })
                            }}
                        >
                            <Form.Item
                                name="consultantName"
                                label="Nama"
                                rules={[{
                                    required: true,
                                    message: "Tidak boleh kosong"
                                }]}
                                hasFeedback
                            >
                                <Input />
                            </Form.Item>
                            <Form.Item
                                name="phoneNumber"
                                label="Nomor Telepon"
                                rules={[{
                                    required: true,
                                    message: "Tidak boleh kosong"
                                }]}
                                hasFeedback
                            >
                                <Input />
                            </Form.Item>
                            <Form.Item>
                                <Button 
                                    type="primary" 
                                    htmlType="submit"
                                    loading={loadingUpdate}
                                    disabled={loadingUpdate}
                                >
                                    Perbarui Konsultan
                                </Button>
                            </Form.Item>
                        </Form>
                    </>
                )
            }
        </>
    )
}

export default EditConsultantPage