import { 
    Button, 
    Form,
    Input,
    Upload,
    InputNumber,
    Typography,
    Switch,
    Spin,
    message,
} from "antd"
import { css } from "@emotion/css";
import { getDocById, updateDocById } from "../../../utils";
import { useParams, useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";
import { getBase64, linkToImage } from "../../../helper/helper";
import { getDownloadURL, ref, uploadBytes } from "firebase/storage";
import { storage } from "../../../config";

const EditProductPage = () => {
    const [form] = Form.useForm()
    const [data, setData] = useState({})
    const [loadingData, setLoadingData] = useState(false)
    const [loadingUpdate, setLoadingUpdate] = useState(false)
    const [imgSrc, setImgSrc] = useState("")

    const { id } = useParams()
    const navigate = useNavigate()

    const getProduct = async () => {
        setLoadingData(true)

        const product = await getDocById("products", id)
        const file = await linkToImage(product.image_product)

        form.setFieldsValue({
            productName: product.productName,
            productDesc: product.description,
            productPrice: product.price,
            productActive: product.IS_ACTIVE,
            productImage: file
        })

        setImgSrc(product.image_product)
        setLoadingData(false)
        setData(product)
    }

    useEffect(() => {
        getProduct()
    }, [])

    const onChange = async (e) => {
        // convert file to base64
        const img = await getBase64(e.file)
        setImgSrc(img)
    }

    return (
        <>
            { 
                loadingData ? (
                    <div
                        className={css`
                            display: flex;
                            justify-content: center;
                            align-items: center;
                            height: 100%;
                        `}
                    >
                        <Spin />
                    </div>
                ) : (
                    <>
                        <Typography.Title level={3}>Edit Produk</Typography.Title>
                        <Form
                            onFinish={async (val) => {
                                setLoadingUpdate(true)

                                const path = `${id}.jpg`
                                const storageRef = ref(storage, `files/${path}`)

                                // get image as a file for uploading to firebase storage
                                const imgFile = val.productImage[0]["originFileObj"] ?? val.productImage[0]

                                const uploadImage = await uploadBytes(storageRef, imgFile)
                                const imgURL = await getDownloadURL(uploadImage.ref)

                                updateDocById("products", id, {
                                    description: val.productDesc,
                                    price: val.productPrice,
                                    productName: val.productName,
                                    image_product: imgURL,
                                    IS_ACTIVE: val.productActive
                                }).then(() => {
                                    setLoadingUpdate(false)

                                    message.success("Edit produk berhasil")

                                    navigate("/admin/product/list")
                                }).catch(() => {
                                    setLoadingUpdate(false)
                                    message.error("Edit produk tidak berhasil")
                                })

                            }}
                            form={form}
                            layout="vertical"
                            initialValues={{
                                productName:"",
                                productImage: [],
                                productPrice: 0,
                                productDesc: "",
                                productActive: false
                            }}
                        >
                            <Form.Item
                                name="productName"
                                label="Nama Produk"
                                rules={[{
                                    required: true,
                                    message: "Tidak boleh kosong"
                                }]}
                                hasFeedback
                            >
                                <Input />
                            </Form.Item>
                            <Form.Item
                                name="productImage"
                                label="Foto Produk"
                                rules={[{
                                    required: true,
                                    message: "Tidak boleh kosong"
                                }]}
                                valuePropName="fileList"
                                getValueFromEvent={(e) => {
                                    if(Array.isArray(e)) return e

                                    return e && e.fileList
                                }}
                            >
                                <Upload 
                                    listType="picture-card" 
                                    showUploadList={false}
                                    onChange={onChange}
                                    beforeUpload={() => false}
                                    multiple={false}
                                    maxCount={1}
                                >
                                    <img 
                                        src={imgSrc}
                                        className={css`
                                            width: 100%;
                                        `}
                                    />
                                </Upload>
                            </Form.Item>
                            <Form.Item
                                name="productDesc"
                                label="Deskripsi"
                                rules={[{
                                    required: true,
                                    message: "Tidak boleh kosong"
                                }]}
                                hasFeedback
                            >
                                <Input.TextArea rows={4} />
                            </Form.Item>
                            <Form.Item
                                name="productPrice"
                                label="Harga"
                                rules={[
                                    {
                                        type:"number",
                                        message: "Harus berupa angka"
                                    },
                                    {
                                        required: true,
                                        message: "Tidak boleh kosong"
                                    }
                                ]}
                                hasFeedback
                            >
                                <InputNumber
                                    className={css`width: 100%;`}
                                    formatter={value => `Rp ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                    parser={value => value.replace(/\Rp\s?|(,*)/g, '')}
                                />
                            </Form.Item>
                            <Form.Item
                                name="productActive"
                                label="Aktif"
                                valuePropName="checked"
                            >
                                <Switch />
                            </Form.Item>
                            <Form.Item>
                                <Button 
                                    type="primary" 
                                    htmlType="submit"
                                    loading={loadingUpdate}
                                    disabled={loadingUpdate}
                                >
                                    Edit Produk
                                </Button>
                            </Form.Item>
                        </Form>
                    </>
                )
            }
        </>
    )
}

export default EditProductPage