import { WechatOutlined } from "@ant-design/icons";
import { logo } from "@assets";
import { css } from "@emotion/css";
import { Button, Typography } from "antd";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { A11y, Navigation, Pagination, Scrollbar } from "swiper";
// Import Swiper styles
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/scrollbar";
import { Swiper, SwiperSlide } from "swiper/react"; // import Swiper core and required modules
import { banner } from "../../../assets";

const { Title } = Typography;

const HomePage = () => {
  const navigate = useNavigate();
  const [information, setInformation] = useState([logo, logo, logo]);
  const [user, setUser] = useState(null);

  const getUser = async () => {
    const data = localStorage.getItem("user");
    setUser(JSON.parse(data));
  };

  useEffect(() => {
    getUser();
  }, []);

  return (
    <div style={styles.content}>
      {/* Greetings */}
      <div>
        <Title style={{ marginBottom: -24, marginTop: -12 }}>
          Good Morning !
        </Title>
        {user && (
          <Title level={3} style={{ marginLeft: 24 }}>
            Mrs. {user.customerName || user.consultantName}
          </Title>
        )}
      </div>
      
      {/* Banner */}
      <img 
        alt="Jafra Banner"
        src={banner}
        className={css`
          width: 100%;
          margin-bottom: 32px;
          border-radius: 5px;
          box-shadow: rgba(99, 99, 99, 0.2) 0px 2px 8px 0px;
        `}
      />

      {/* Chat */}
      {user?.role === "CUSTOMER" ? (
        <Button
          style={styles.buttonChat}
          onClick={() => navigate("/chat", { replace: true })}
        >
          <WechatOutlined style={{ fontSize: 48 }} />
          Chat With Consultant !
        </Button>
      ) : (
        <Button
          style={styles.buttonChat}
          onClick={() => navigate("/consultant-chat", { replace: true })}
        >
          <WechatOutlined style={{ fontSize: 48 }} />
          Chat With Customer !
        </Button>
      )}
    </div>
  );
};

export default HomePage;

const styles = {
  content: {
    display: "flex",
    flexDirection: "column",
    height: "100%",
  },
  imageSwipper: {
    display: "flex",
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
  },
  buttonChat: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "flex-end",
    height: "auto",
    borderWidth: 0,
  },
};
