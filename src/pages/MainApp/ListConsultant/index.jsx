import { Table, Space, Button, Typography, Modal, message } from "antd"
import { Link } from "react-router-dom"
import { css } from "@emotion/css"
import { useState } from "react"
import { userCollectionRef } from "../../../config"
import { useEffect } from "react"
import { getDocs, query, where } from "firebase/firestore"
import { deleteDocById } from "../../../utils"

const ListConsultantPage = () => {
    const [loadingData, setLoadingData] = useState(false)
    const [consultants, setConsultants] = useState([])

    const fetchConsultants = async () => {
        const data = []
        setLoadingData(true)

        const q = query(userCollectionRef, where("role", "==", "CONSULTANT"))

        const querySnapshot = await getDocs(q)
        querySnapshot.forEach((doc) => data.push(doc.data()))

        setLoadingData(false)
        setConsultants(data)
    }

    const deleteConsultant = (id) => {
        Modal.confirm({
            title: "Apakah kamu ingin menghapus produk ini ?",
            content: "Data yang sudah dihapus tidak akan bisa kembali lagi",
            okText: "Hapus",
            cancelText: "Kembali",
            onOk: () => {
                message.loading("Menghapus data konsultan...")
                deleteDocById("users", id)
                    .then(async () => {
                        message.destroy()

                        // Refetch product data
                        fetchConsultants()

                        message.success("Konsultan berhasil dihapus")
                    }).catch(() => {
                        message.destroy()

                        message.error("Konsultan gagal dihapus")
                    })
            }
        })
    }

    useEffect(() => fetchConsultants(), [])

    return (
        <>
            <div 
                className={css`
                    display: flex;
                    justify-content: space-between;
                    align-items: center;
                    margin-bottom: 16px;
                `}
            >
                <Typography.Title level={3}>Daftar Konsultan</Typography.Title>
                <Link to="../consultant/add">
                    <Button type="primary">
                        Tambah Konsultan
                    </Button>
                </Link>
            </div>
            <Table 
                dataSource={
                    consultants.map((consultant, i) => ({
                        key: i,
                        name: consultant.consultantName,
                        phoneNumber: consultant.phoneNumber,
                        consultantID: consultant.consultantID
                    }))
                }
                loading={loadingData}
            >
                <Table.Column title="Nama Konsultan" key="name" dataIndex="name" />
                <Table.Column title="Nomor Telepon" key="phoneNumber" dataIndex="phoneNumber" />
                <Table.Column 
                    title="Aksi" 
                    dataIndex="aksi" 
                    key="aksi"
                    render={(_, record) => (
                        <Space>
                            <Link to={`../consultant/edit/${record.consultantID}`}>
                                <Button type="primary">
                                    Edit
                                </Button>
                            </Link>
                            <Button
                                type="primary"
                                danger
                                onClick={() => deleteConsultant(record.consultantID)}
                            >
                                Hapus
                            </Button>
                        </Space>
                    )}
                />
            </Table>
        </>
    )
}

export default ListConsultantPage