import { css } from "@emotion/css"
import { 
    Button, 
    Image, 
    message, 
    Space, 
    Switch, 
    Table, 
    Typography,
    Modal 
} from "antd"
import { deleteObject, ref } from "firebase/storage"
import { useEffect, useState } from "react"
import { Link } from "react-router-dom"
import { productCollectionRef, storage } from "../../../config"
import { deleteDocById, getFromCollection } from "../../../utils"

const ListProductPage = () => {
    const [products, setProducts] = useState([])
    const [loadingData, setLoadingData] = useState(false)

    const fetchProducts = async () => {
        setLoadingData(true)

        const data = await getFromCollection(productCollectionRef)

        setLoadingData(false)
        setProducts(data)
    }

    const deleteProduct = (id) => {
        Modal.confirm({
            title: "Apakah kamu ingin menghapus produk ini ?",
            content: "Data yang sudah dihapus tidak akan bisa kembali lagi",
            okText: "Hapus",
            cancelText: "Kembali",
            onOk: () => {
                message.loading("Menghapus produk...")
                deleteDocById("products", id)
                    .then(async () => {
                        message.destroy()

                        const productImagePath = `${id}.jpg`
                        const storageRef = ref(storage, `files/${productImagePath}`)

                        // also delete existing image in storage
                        await deleteObject(storageRef)

                        // Refetch product data
                        fetchProducts()

                        message.success("Produk berhasil dihapus")
                    }).catch(() => {
                        message.destroy()

                        message.error("Produk gagal dihapus")
                    })
            }
        })
    }

    useEffect(() => {
        fetchProducts()
    },[])

    return (
        <>
            <div 
                className={css`
                    display: flex;
                    justify-content: space-between;
                    align-items: center;
                    margin-bottom: 16px;
                `}
            >
                <Typography.Title level={3}>Daftar Produk</Typography.Title>
                <Link to="../product/add">
                    <Button type="primary">
                        Tambah Produk
                    </Button>
                </Link>
            </div>
            <Table 
                loading={loadingData}

                dataSource={
                    products.map((product,i) =>({
                        key: i,
                        nama: product.productName,
                        harga: product.price,
                        foto: product.image_product,
                        productID: product.productID,
                        aktif: product.IS_ACTIVE
                    }))
                }
            >
                <Table.Column title="Nama" dataIndex="nama" key="nama"/>
                <Table.Column 
                    title="Foto" 
                    dataIndex="foto" 
                    key="foto"
                    render={(_, record) => (
                        <Image src={record.foto} width={100} height={100} />
                    )}
                />
                <Table.Column title="Harga" dataIndex="harga" key="harga"/>
                <Table.Column 
                    title="Aktif"
                    dataIndex="aktif"
                    key="aktif"
                    render={(_, record) => (
                        <Switch 
                            checked={record.aktif}
                        />
                    )}
                />
                <Table.Column 
                    title="Aksi" 
                    dataIndex="aksi" 
                    key="aksi"
                    render={(_, record) => (
                        <Space>
                            <Link to={`../product/edit/${record.productID}`}>
                                <Button type="primary">
                                    Edit
                                </Button>
                            </Link>
                            <Button
                                type="primary"
                                danger
                                onClick={() => deleteProduct(record.productID)}
                            >
                                Hapus
                            </Button>
                        </Space>
                    )}
                />
            </Table>
        </>
    )
}

export default ListProductPage