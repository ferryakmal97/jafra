import React, { useState } from "react";
import { SendOutlined } from "@ant-design/icons";
import { Modal, Button, Tag } from "antd";
import { useNavigate } from "react-router-dom";

const Card = ({ data, style }) => {
  const navigate = useNavigate();
  const user = JSON.parse(localStorage.getItem("user"));
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [selected, setSelected] = useState({});
  const showModal = () => {
    setSelected(data);
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setSelected({});
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setSelected({});
    setIsModalVisible(false);
  };
  return (
    <div
      style={{
        height: "35%",
        width: "15%",
        ...style,
      }}
    >
      <div
        style={{
          display: "flex",
          width: "100%",
          height: "100%",
          marginBottom: 10,
          position: "relative",
        }}
      >
        <img
          src={data.image_product}
          style={{
            display: "flex",
            width: "100%",
            height: "100%",
          }}
          alt=""
        />
        <div
          style={{
            position: "absolute",
            right: 0,
            bottom: 5,
          }}
        >
          {data.IS_ACTIVE ? (
            <Tag color="#87d068">Available</Tag>
          ) : (
            <Tag color="#f50">Not Available</Tag>
          )}
        </div>
      </div>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <Button type="primary" block onClick={showModal}>
          Cek Detail
        </Button>
        {user.role === "CUSTOMER" && (
          <>
            <div style={{ width: "20%" }} />
            <Button
              onClick={() => navigate("/chat", { replace: true })}
              type="primary"
              style={{ alignSelf: "center", width: "30%" }}
              icon={<SendOutlined />}
            />
          </>
        )}
      </div>
      <>
        <Modal
          title={selected.productName}
          visible={isModalVisible}
          onOk={handleOk}
          onCancel={handleCancel}
        >
          <p>{selected.description}</p>
        </Modal>
      </>
    </div>
  );
};

export default Card;
