import React, { useEffect, useState } from "react";
import { productCollectionRef } from "../../../config";
import { getFromCollection } from "../../../utils";
import Card from "./Card";

const ProductPage = () => {
  const [products, setProducts] = useState([]);

  const get = async () => {
    const data = await getFromCollection(productCollectionRef);
    setProducts(data);
  };

  useEffect(() => {
    get();
  }, []);
  return (
    <div style={{ ...styles.container }}>
      {products.map((value, index) => (
        <Card data={value} key={index} style={{ paddingRight: 10 }} />
      ))}
    </div>
  );
};

export default ProductPage;

const styles = {
  container: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "center",
    paddingTop: 20,
    paddingBottom: 20,
  },
};
