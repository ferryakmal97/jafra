import HomePage from "./Home";
import ChatPage from "./Chat";
import ProductPage from "./Products";
import AddProductPage from "./AddProduct";
import ListProductPage from "./ListProduct";
import EditProductPage from "./EditProduct";
import AddConsultantPage from "./AddConsultant";
import EditConsultantPage from "./EditConsultant";
import ListConsultantPage from "./ListConsultant";
import DashboardAdminPage from "./DashboardAdmin";
import ConsultantChatPage from "./ConsultantChat";
import ConsultantPage from "./Consultants";

export {
  HomePage,
  ChatPage,
  ProductPage,
  AddProductPage,
  ListProductPage,
  EditProductPage,
  AddConsultantPage,
  EditConsultantPage,
  ListConsultantPage,
  DashboardAdminPage,
  ConsultantChatPage,
  ConsultantPage,
};
