import React from "react";
import { Button, Input, message, Tooltip, Typography } from "antd";
import {
  MailOutlined,
  KeyOutlined,
  EyeTwoTone,
  EyeInvisibleOutlined,
  InfoCircleOutlined,
} from "@ant-design/icons";
import { useFormik } from "formik";
import * as Yup from "yup";
import { styles } from "../styles";
import { signInWithEmailAndPassword } from "firebase/auth";
import { auth } from "../../../config";
import { getDocById } from "../../../utils/services";
import { useNavigate } from "react-router-dom";

const { Title, Text, Link } = Typography;

const LoginPage = () => {
  const navigate = useNavigate();
  const onSubmit = (form) => {
    const { email, password } = form;
    signInWithEmailAndPassword(auth, email, password)
      .then(async (userCredential) => {
        // Signed in
        const user = userCredential.user;
        // ...
        const userData = await getDocById("users", user.uid);
        localStorage.setItem("user", JSON.stringify(userData));
        message.success("Login Success !");
        if (userData.role === "ADMIN") {
          navigate("/admin", { replace: true });
        } else {
          navigate("/", { replace: true });
        }
      })
      .catch((error) => {
        console.log(error);
        message.error("Login Failed !");
      })
      .finally(() => {
        formik.setSubmitting(false);
      });
  };

  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    validationSchema: Yup.object({
      email: Yup.string().required("Email must be filled !"),
      password: Yup.string().required("Password must be filled !"),
    }),
    onSubmit: onSubmit,
  });

  return (
    <div style={styles.container}>
      <div style={styles.formContainer}>
        <div style={{ height: "10%" }} />
        <Title>Sign In</Title>
        <Input
          size="large"
          name="email"
          value={formik.values.email}
          onChange={formik.handleChange}
          placeholder="Email"
          prefix={<MailOutlined />}
          suffix={
            formik.errors.email &&
            formik.touched.email && (
              <Tooltip title={formik.errors.email}>
                <InfoCircleOutlined style={{ color: "rgba(0,0,0,.45)" }} />
              </Tooltip>
            )
          }
        />
        <Input.Password
          size="large"
          name="password"
          value={formik.values.password}
          onChange={formik.handleChange}
          placeholder="Password"
          prefix={<KeyOutlined />}
          iconRender={(visible) =>
            visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
          }
        />
        {formik.errors.password && formik.touched.password && (
          <Text type="danger">{formik.errors.password}</Text>
        )}
        <Button
          block
          type="primary"
          onClick={formik.handleSubmit}
          loading={formik.isSubmitting}
        >
          Sign In
        </Button>
        <Text>
          Don't have an account ? <Link href="/register">Sign up here</Link>
        </Text>
        <div style={{ height: "10%" }} />
      </div>
    </div>
  );
};

export default LoginPage;
