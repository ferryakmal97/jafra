import React from "react";
import { Button, Input, Tooltip, Typography, message } from "antd";
import {
  MailOutlined,
  KeyOutlined,
  EyeTwoTone,
  EyeInvisibleOutlined,
  UserOutlined,
  InfoCircleOutlined,
  PhoneOutlined,
} from "@ant-design/icons";
import { useFormik } from "formik";
import * as Yup from "yup";
import { styles } from "../styles";
import { createUserWithEmailAndPassword } from "firebase/auth";
import { auth } from "../../../config";
import { addDataFixId } from "../../../utils/services";

const { Title, Text, Link } = Typography;

const phoneRegExp =
  /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;

const RegisterPage = () => {
  const onSubmit = (form) => {
    const { email, password, name, phoneNumber } = form;
    createUserWithEmailAndPassword(auth, email, password)
      .then((userCredential) => {
        const user = userCredential.user;
        const data = {
          email,
          customerName: name,
          phoneNumber,
          customerID: user.uid,
          customer_created: user.metadata.creationTime,
          role: "CUSTOMER",
        };
        addDataFixId("users", user.uid, data);
        message.success("Account successfully created !");
        formik.resetForm();
      })
      .catch((error) => {
        console.log(error);
        message.error("Failed create account !");
      })
      .finally(() => {
        formik.setSubmitting(false);
      });
  };

  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
      name: "",
      phoneNumber: "",
    },
    validationSchema: Yup.object({
      email: Yup.string()
        .email("Invalid email format")
        .required("Email must be filled !"),
      password: Yup.string()
        .min(8, "Minimum 8 characters")
        .required("Password must be filled !"),
      name: Yup.string()
        .min(3, "Mininum 3 characters")
        .max(15, "Maximum 15 characters")
        .required("Name must be filled !"),
      phoneNumber: Yup.string().matches(
        phoneRegExp,
        "Phone number is not valid",
      ),
    }),
    onSubmit: onSubmit,
  });

  return (
    <div style={styles.container}>
      <div style={styles.formContainer}>
        <Title>Sign Up</Title>
        <Input
          size="large"
          name="name"
          value={formik.values.name}
          onChange={formik.handleChange}
          placeholder="Full Name"
          prefix={<UserOutlined />}
          suffix={
            formik.errors.name &&
            formik.touched.name && (
              <Tooltip title={formik.errors.name}>
                <InfoCircleOutlined style={{ color: "rgba(0,0,0,.45)" }} />
              </Tooltip>
            )
          }
        />
        <Input
          size="large"
          name="email"
          value={formik.values.email}
          onChange={formik.handleChange}
          placeholder="Email"
          prefix={<MailOutlined />}
          suffix={
            formik.errors.email &&
            formik.touched.email && (
              <Tooltip title={formik.errors.email}>
                <InfoCircleOutlined style={{ color: "rgba(0,0,0,.45)" }} />
              </Tooltip>
            )
          }
        />
        <Input
          size="large"
          name="phoneNumber"
          value={formik.values.phoneNumber}
          onChange={formik.handleChange}
          placeholder="Phone Number"
          prefix={<PhoneOutlined />}
          suffix={
            formik.errors.phoneNumber &&
            formik.touched.phoneNumber && (
              <Tooltip title={formik.errors.phoneNumber}>
                <InfoCircleOutlined style={{ color: "rgba(0,0,0,.45)" }} />
              </Tooltip>
            )
          }
        />
        <Input.Password
          size="large"
          name="password"
          value={formik.values.password}
          onChange={formik.handleChange}
          placeholder="Password"
          prefix={<KeyOutlined />}
          iconRender={(visible) =>
            visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
          }
        />
        {formik.errors.password && formik.touched.password && (
          <Text type="danger">{formik.errors.password}</Text>
        )}
        <Button
          block
          type="primary"
          onClick={formik.handleSubmit}
          loading={formik.isSubmitting}
        >
          Register
        </Button>
        <Text>
          Have an account ? <Link href="/login">Sign in here</Link>
        </Text>
      </div>
    </div>
  );
};

export default RegisterPage;
