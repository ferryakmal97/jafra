import {
  arrayUnion,
  collection,
  deleteDoc,
  doc,
  getDoc,
  getDocs,
  query,
  setDoc,
  updateDoc,
  where,
} from "firebase/firestore";
import { db } from "../../config";

export const getFromCollection = async (col) => {
  const querySnapshot = await getDocs(col);
  const data = [];
  querySnapshot.forEach((doc) => {
    data.push(doc.data());
  });
  return data;
};

export const getWithQuery = async (col, ...condition) => {
  const q = query(collection(db, col), where(...condition));
  const querySnapshot = await getDocs(q);
  const data = [];
  querySnapshot.forEach((doc) => {
    data.push(doc.data());
  });
  return data;
};

export const addDataFixId = async (col, id, data) => {
  await setDoc(doc(db, col, id), data);
};

export const getDocById = async (col, id) => {
  const docRef = doc(db, col, id);
  const docSnap = await getDoc(docRef);

  if (docSnap.exists()) {
    return docSnap.data();
  } else {
    // doc.data() will be undefined in this case
    return null;
  }
};

export const sendFirstMsg = async (data) => {
  const newMsg = doc(collection(db, "messages"));
  await setDoc(newMsg, { ...data, msgId: newMsg.id });
};

export const sendMsg = async (data, id) => {
  await updateDoc(doc(db, "messages", id), { messages: arrayUnion(data) });
};
export const updateDocById = async (col, id, data) => {
  const docRef = doc(db, col, id);

  return await updateDoc(docRef, data);
};

export const deleteDocById = async (col, id) => {
  const docRef = doc(db, col, id);

  return await deleteDoc(docRef);
};

export const createDoc = async (docRef, data) => await setDoc(docRef, data);
